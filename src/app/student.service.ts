import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class StudentService {

 // private baseUrl = 'http://localhost:8080/api/';

  constructor(private http:HttpClient) { }
  
  getStudentList(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/students-list`);
  }
  
  createStudent(student: object): Observable<object> {
    return this.http.post(`${environment.apiUrl}/api/save-student`, student);
  }

  deleteStudent(id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/api/delete-student/${id}`, { responseType: 'text' });
  }

  getStudent(id: number): Observable<Object> {
    return this.http.get(`${environment.apiUrl}/api/student/${id}`);
  }

  updateStudent(id: number, value: any): Observable<Object> {
    return this.http.post(`${environment.apiUrl}/api/update-student/${id}`, value);
  }
}