import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from '../_services';
import { Router } from '@angular/router';
import { User } from '../_models';
import { Observable, Subject } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  selectFormControl:any = [];

  constructor(private userservice: UserService, private router: Router,private authenticationService: AuthenticationService) {

    //this.getRoleType();
   }


   getRoleType() {
    this.selectFormControl = [];
    this.authenticationService.getRoleType().subscribe((data: {}) => {
      this.selectFormControl = data;
      console.log(this.selectFormControl);
    });
  }
  usersArray: any[] = [];
  dtTrigger: Subject<any> = new Subject();


  users: Observable<User[]>;
  user: User = new User();
  deleteMessage = false;
  userlist: any;
  isupdated = false;
  ngOnInit() {
if (localStorage.length > 0) {
      this.isupdated = false;
      this.userservice.getUserList().subscribe(data => {
      this.users = data;
      this.dtTrigger.next();
      })
      } else {
        this.router.navigate(['/login']);
      }
  }
  deleteUser(id: number) {
    this.userservice.deleteUser(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage = true;
          // tslint:disable-next-line: no-shadowed-variable
          this.userservice.getUserList().subscribe( data => {
            console.log(data);
            this.users = data;
            })},
        error => console.log(error));
  }
  updateUser(id: number) {
    this.userservice.getUser(id)
      .subscribe(
        data => {
          console.log(data);
          this.userlist = data;
       },
        error => console.log(error));
  }
                            userupdateform = new FormGroup({id: new FormControl(),
                                                            username: new FormControl(),
                                                              password: new FormControl(),
                                                              firstname: new FormControl(),
                                                              lastname: new FormControl(),
                                                              token: new FormControl(),
                                                              role: new FormControl()  
                                                            });

  updateUse(updateUse) {
  this.user = new User();
  this.user.id = this.UserId.value;
  this.user.username = this.Userusername.value;
  this.user.password = this.Userpassword.value;
  this.user.firstName = this.UserfirstName.value;
  this.user.lastName = this.UserlastName.value;
  this.user.token = this.Usertoken.value;
  this.user.role = this.Userrole.value;


  this.userservice.updateUser(this.user.id,this.user).subscribe(
data => {
      this.isupdated = true;
      this.userservice.getUserList().subscribe(data => {
        this.users = data;
        })
    },
error => console.log(error));
}

get Userusername() {
  return this.userupdateform.get('username');
}
get Userpassword() {
  return this.userupdateform.get('password');
}
get UserfirstName() {
  return this.userupdateform.get('firstName');
}
get UserlastName() {
  return this.userupdateform.get('lastName');
}
get Usertoken() {
  return this.userupdateform.get('token');
}
get Userrole() {
  return this.userupdateform.get('role');
}
get UserId() {
  return this.userupdateform.get('id');
}
changeisUpdate() {
  this.isupdated = false;
}

logout() {
  console.log('logout');
  this.authenticationService.logout();
}
view_Student() {
  this.router.navigate(['/view-student']);
}
add_Student() {
  this.router.navigate(['/add-student']);
}
view_User()
  {
    this.router.navigate(['/view-user']);
  }
  add_User()
  {
    this.router.navigate(['/add-user']);
  }
}

