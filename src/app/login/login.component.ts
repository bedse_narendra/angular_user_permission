import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first, tap } from 'rxjs/operators';
import { AuthenticationService, AlertService } from '../_services';
import { User } from '../_models';
import { throwError } from 'rxjs';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  invalidLogin = false;
  results: User;
  errorMessage: any;
  selectFormControl:any = [];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService)

{
    // redirect to home if already logged in
    // if (this.authenticationService.currentUserValue) {
    //     this.router.navigate(['/']);
    // }
}



ngOnInit() {
    this.getRoleType();
    this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
       role: ['', Validators.required],
});

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
}

getRoleType() {
    this.selectFormControl = [];
    this.authenticationService.getRoleType().subscribe((data: {}) => {
      this.selectFormControl = data;
      console.log(this.selectFormControl);
    });
  }

// convenience getter for easy access to form fields
get f() { return this.loginForm.controls; }

onSubmit() {
  //console.log(this.role);
  this.submitted = true;
  this.authenticationService.login(this.f.username.value, this.f.password.value,this.f.role.value).pipe(first()).subscribe(
            data => {
            if(data) {
                console.log('entered if',data);
                this.router.navigate(['/dashboard']);
                this.invalidLogin = false;
               // this.router.navigate([this.returnUrl]);
              } else {
                    console.log('entered else ',data);
                    this.router.navigate(['/login']);
                    this.invalidLogin = true;
                    alert('Please enter correct Username or Password..!');
             }
            },
           );
        }
    }
function error(message) {
        return throwError({ error: { message } });
    }