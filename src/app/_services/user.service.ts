import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../_models';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/api`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/api/${id}`);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}/api/register`, user);
    }

    update(user: User) {
        return this.http.put(`${environment.apiUrl}/api/${user.id}`, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/${id}`);
    }


    getUserList(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/api/user-list`);
      }

      createUser(student: object): Observable<object> {
        return this.http.post(`${environment.apiUrl}/api/save-user`, student);
      }

      deleteUser(id: number): Observable<any> {
        return this.http.delete(`${environment.apiUrl}/api/delete-user/${id}`, { responseType: 'text' });
      }

      getUser(id: number): Observable<Object> {
        return this.http.get(`${environment.apiUrl}/api/user/${id}`);
      }

      updateUser(id: number, value: any): Observable<Object> {
        return this.http.post(`${environment.apiUrl}/api/update-user/${id}`, value);
      }
    }