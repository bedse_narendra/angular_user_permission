import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( private router: Router,private authenticationService: AuthenticationService) { }

  ngOnInit() {
    if (localStorage.length > 0) {
      } else {
        this.router.navigate(['/login']);
      }
  }
  logout()
  {
    console.log('logout');
    this.authenticationService.logout();
  }
  view_Student()
  {
    this.router.navigate(['/view-student']);
  }
  add_Student(){
    this.router.navigate(['/add-student']); 
  }
  view_User()
  {
    this.router.navigate(['/view-user']);
  }
  add_User()
  {
    this.router.navigate(['/add-user']);
  }
}
